﻿using appJogos.Shared;
using appJogos.Shared.Enumeradores;
using System;

namespace appJogos.Domain.Model
{
    public class JogosModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public EnumEstiloJogos EstiloJogo { get; set; }
        public string Observacao { get; set; }


        public override string ToString()
        {
            return $"{ Id } - {Nome} - { EstiloJogo.ToString()} ";
        }

        public void GerarId()
        {
            Id = Util.GerarId();
        }

    }

    
}
