﻿using appJogos.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace appJogos.Domain.Model
{
    public class GerenciarEmprestimoModel
    {
        public int Id { get; set; }
        public int? AmigoId { get; set; }
        public AmigoModel Amigo { get; set; }
        public int? JogoId{ get; set; }
        public JogosModel Jogos { get; set; }

        public void GerarId()
        {
            Id = Util.GerarId();
        }
    }
}
