﻿using appJogos.Shared;
using appJogos.Shared.Enumeradores;
using System;

namespace appJogos.Domain.Model
{
    public class AmigoModel
    {
        public  int Id { get; set; }
        public string Nome { get; set; }
        public EnumSexo Sexo { get; set; }
        public string DescricaoSexo
        {
            get {
                return Sexo.Equals(EnumSexo.Masculino) ? "Masculino" : "Feminino";
            }
        }

        public override string ToString()
        {
            return $"{Id} - {Nome } -{DescricaoSexo}";
        }
        public void GerarId()
        {
            Id = Util.GerarId();
        }

    }
}
