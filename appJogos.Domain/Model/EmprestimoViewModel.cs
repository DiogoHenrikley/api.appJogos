﻿using appJogos.Shared.Enumeradores;

namespace appJogos.Domain.Model
{
    public class EmprestimoViewModel
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public string NomeAmigo { get; set; }
        public int JogoId { get; set; }
        public string NomeJogo { get; set; }
        public EnumEstiloJogos EstiloJogo { get; set; }
        public string StatusJogo { get { return !string.IsNullOrEmpty(NomeJogo) ? "Emprestado" : "Dispnoível"; } }
    }
}
