﻿using appJogos.Shared;
using System.Text.Json.Serialization;

namespace appJogos.Domain.Model
{
    public class UsuarioModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Token { get; set; }

        [JsonIgnore]
        public bool IsValid
        {
            get
            {
                return (!string.IsNullOrEmpty(Nome) && !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Senha));
            }
        }

        public void GerarId()
        {
            Id = Util.GerarId();
        }
    }
}
