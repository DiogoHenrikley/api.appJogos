﻿using appJogos.Domain.Model;
using appJogos.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace appJogos.Test
{
    public class TestFixture<TStartup> : IDisposable
    {
        private TestServer _server;
        public HttpClient Client;
        public string UrlServidor = "http://localhost:8089";
        public static string Token = string.Empty;


        public static string GetProjetPath(string caminhoRelativoProjeto, Assembly startupAssembly)
        {
            var nomeProjeto = startupAssembly.GetName().Name;
            var caminhoBaseAplicacao = AppContext.BaseDirectory;
            var diretorioInfo = new DirectoryInfo(caminhoBaseAplicacao);
            do
            {
                diretorioInfo = diretorioInfo.Parent;
                var informacaoDiretorioProjeto = new DirectoryInfo(Path.Combine(diretorioInfo.FullName, caminhoRelativoProjeto));
                if (informacaoDiretorioProjeto.Exists)
                    if (new FileInfo(Path.Combine(informacaoDiretorioProjeto.FullName, nomeProjeto, $"{nomeProjeto}.csproj")).Exists)
                        return Path.Combine(informacaoDiretorioProjeto.FullName, nomeProjeto);
            } while (diretorioInfo.Parent != null);
            throw new FileNotFoundException($"Não foi possível carregar o projeto {nomeProjeto} localizado no caminho {caminhoBaseAplicacao} ");
        }

        public TestFixture() : this(Path.Combine(""))
        {

        }

        protected virtual void InitializeServices(IServiceCollection services)
        {
            var startupAssembly = typeof(TStartup).GetTypeInfo().Assembly;
            var manager = new ApplicationPartManager
            {
                ApplicationParts =
                {
                    new AssemblyPart(startupAssembly)
                },
                FeatureProviders =
                {
                    new ControllerFeatureProvider(),
                    new ViewComponentFeatureProvider()
                }
            };
            services.AddSingleton(manager);
        }

        protected TestFixture(string relativeTargetProjectParent)
        {
            var startupAssembly = typeof(TStartup).GetTypeInfo().Assembly;
            var contentRoot = GetProjetPath(relativeTargetProjectParent, startupAssembly);
            var configurationBuilder = new ConfigurationBuilder()
                                                                .SetBasePath(contentRoot)
                                                                .AddJsonFile("appsettings.json");
            var webHostBuilder = new WebHostBuilder()
                                     .UseContentRoot(contentRoot)
                                     .ConfigureServices(InitializeServices)
                                     .UseConfiguration(configurationBuilder.Build())
                                     .UseEnvironment("Development")
                                     .UseStartup(typeof(TStartup));

            _server = new TestServer(webHostBuilder);

            Client = _server.CreateClient();
            Client.BaseAddress = new Uri(UrlServidor);
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
            if (string.IsNullOrEmpty(Token))
            {
                Token = GerarTokenMaxSolucoes().Result;
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            }
        }


        private async Task<string> GerarTokenMaxSolucoes()
        {
            //usuario = maxima.adm e senha : asd123 = > criptografado na lib maxima45
            var obj = new { login = "ac1dLct4pfST4LfsCZbFCg==", password = "kyljmv6kpEZSFiKKucHGtA==" };
            var usuario = new UsuarioModel()
            {
                Id = Util.GerarId(),
                Nome = "Diogo Henrikley F Martins",
                Email = "DiogoHenrikley@gmail.com",
                Senha = "1234"
            };

            var client = new HttpClient()
            {
                BaseAddress = new Uri("https://localhost:5001/api/Login/GerarToken"),
            };

            var content = new StringContent(JsonConvert.SerializeObject(usuario));

            var response = await client.PutAsync(client.BaseAddress.ToString(), content);
            var objToken = response.Content.ReadAsStringAsync().Result;
            var retorno = JsonConvert.DeserializeObject<UsuarioModel>(objToken);

            return retorno.Token;
        }


        public void Dispose()
        {
            Client.Dispose();
            _server.Dispose();
        }

    }
}
