using System.Net.Http;
using Xunit;

namespace appJogos.Test
{
    public class AmigosTest : IClassFixture<TestFixture<Startup>>
    {
        /*
         * Theory - > Usado para m�todo que possuem parametros e deve ser usado com outro parametro [InlineData]
         * Fact   - > Usado para m�todos que n�o possuem parametros e n�o precisa do [InlineData]
         */

        private HttpClient HttpClient;

        public AmigosTest(TestFixture<Startup> fixture)
        {
            HttpClient = fixture.Client;
            fixture.UrlServidor = "http://localhost:8089/";
        }

        [Fact(DisplayName ="")]
        public  async void ObterAmigo()
        {
            var amigo = await  HttpClient.GetAsync("https://localhost:5001/api/Amigos");
            var ret = await amigo.Content.ReadAsStringAsync();

            Assert.NotNull(ret);
        }

       


        [Fact]
        public void Dispose()
        {
            this.Dispose();
        }

    }
}
