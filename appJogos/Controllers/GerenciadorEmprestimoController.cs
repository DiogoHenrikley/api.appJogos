﻿using appJogos.Domain.Model;
using appJogos.Repository.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace appJogos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GerenciadorEmprestimoController : BaseController
    {
        private readonly IGerenciarEmprestimoRepository _gerenciarEmprestimo;
        private readonly IEmprestimoRepository _emprestimoRepository;
        private readonly IJogosRepository _jogosRepository;
        private readonly IAmigosRepository _amigosRepository;
        public GerenciadorEmprestimoController(IGerenciarEmprestimoRepository gerenciarEmprestimo,
                                               IEmprestimoRepository emprestimoRepository,
                                               IJogosRepository jogosRepository,
                                               IAmigosRepository amigosRepository)
        {
            _gerenciarEmprestimo = gerenciarEmprestimo;
            _emprestimoRepository = emprestimoRepository;
            _jogosRepository = jogosRepository;
            _amigosRepository = amigosRepository;
        }

        [HttpGet]
        public async Task<IActionResult> ObterTodos()
        {
            try
            {
                var lista = await _gerenciarEmprestimo.ObterTodos();

                var novalista = from ls in lista
                                select new EmprestimoViewModel()
                                {
                                    Id = ls.Id,
                                    EstiloJogo = ls.Jogos.EstiloJogo,
                                    NomeJogo = ls.Jogos.Nome,
                                    NomeAmigo = ls.Amigo.Nome
                                };

                return ResultadoPesquisa(novalista.ToList());
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Adicionar([FromBody] EmprestimoModel entity)
        {
            try
            {
                var gerEmp = new GerenciarEmprestimoModel()
                {
                    Amigo = await _amigosRepository.ObterPorId(entity.AmigoId),
                    Jogos = await _jogosRepository.ObterPorId(entity.JogoId),
                    AmigoId = entity.AmigoId,
                    JogoId = entity.JogoId
                };
                gerEmp.GerarId();

                var jogo = await _gerenciarEmprestimo.ObterJogoPorId(entity.JogoId);
                if (jogo != null)
                {
                    var amigo = await _amigosRepository.ObterPorId(Convert.ToInt32(jogo.AmigoId));
                    return Response($"O jogo {jogo.Jogos.Nome} já está emprestado para {amigo.Nome}", false);
                }

                var result = await _gerenciarEmprestimo.Adicionar(gerEmp);

                return Response(result);
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> ObterPorId(int Id)
        {
            try
            {
                return Response(await _gerenciarEmprestimo.ObterPorId(Id));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpGet("ObterPorDescricao/{Descricao}")]
        public async Task<IActionResult> ObterPorDescricao(string Descricao)
        {
            try
            {
                var result = await _gerenciarEmprestimo.Buscar(b => b.Jogos.Nome.ToUpper().Contains(Descricao.ToUpper()) ||
                                                        b.Amigo.Nome.ToUpper().Contains(Descricao.ToUpper()));
                return ResultadoPesquisa(result.ToList());
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpPut]
        public async Task<IActionResult> Atualizar([FromBody] EmprestimoModel entity)
        {
            try
            {
                return Response(await _emprestimoRepository.Atualizar(entity));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Remover(int Id)
        {
            try
            {
                var entity = await _gerenciarEmprestimo.ObterPorId(Id);
                return Response(await _gerenciarEmprestimo.Remover(entity));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }

        }

    }
}
