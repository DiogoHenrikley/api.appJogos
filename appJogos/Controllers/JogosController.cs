﻿using appJogos.Domain.Model;
using appJogos.Repository.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace appJogos.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    //[Authorize]
    public class JogosController : BaseController
    {
        private readonly IJogosRepository _jogosRepository;

        public JogosController(IJogosRepository jogosRepository)
        {
            _jogosRepository = jogosRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Adicionar([FromBody]JogosModel entity)
        {
            try
            {

                entity.GerarId();

                return Response(await _jogosRepository.Adicionar(entity));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }
        [HttpPut]
        public async Task<IActionResult> Atualizar([FromBody]JogosModel entity)
        {
            try
            {
                return Response(await _jogosRepository.Atualizar(entity));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpGet("obterPorDescricao/{Descricao}")]
        public async Task<IActionResult> ObterPorDescricao(string Descricao)
        {
            try
            {
                return ResultadoPesquisa(await _jogosRepository.Buscar(b => b.Nome.ToUpper().Contains(Descricao.ToUpper())));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }
        
        [HttpGet("{Id}")]
        public async Task<IActionResult> ObterPorId(int Id)
        {
            try
            {
                return Response(await _jogosRepository.ObterPorId(Id));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
            throw new System.NotImplementedException();
        }

        [HttpGet]
        public async Task<IActionResult> ObterTodos()
        {
            try
            {
                return ResultadoPesquisa(await _jogosRepository.ObterTodos());
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
            throw new System.NotImplementedException();
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Remover(int Id)
        {
            try
            {
                var entidade = await _jogosRepository.ObterPorId(Id);
                return Response(await _jogosRepository.Remover(entidade));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
            throw new System.NotImplementedException();
        }
    }
}
