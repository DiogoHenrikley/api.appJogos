﻿using appJogos.Domain.Model;
using appJogos.Repository.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace appJogos.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class LoginController : BaseController
    {
        private readonly IUsuarioRepository _usuarioRepository;
        public LoginController(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }
        [HttpPost("Autenticar")]
        public async Task<IActionResult> Autenticar([FromBody] LoginModel login)
        {
            try
            {
                var usuario = await _usuarioRepository.Autenticar(login.Email, login.Password);
                if (usuario == null)
                    return Response("Usuário não encontrado", false);
                usuario.Token = Token.GerarToken(usuario);
                return Response(usuario);

            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpPost("GerarUsuario")]
        public async Task<IActionResult> SalvarUsuario([FromBody] UsuarioModel usuario)
        {
            try
            {
                if (usuario.IsValid)
                {
                    usuario.GerarId();
                    var user = await _usuarioRepository.Buscar(b => b.Email.ToUpper().Equals(usuario.Email.ToUpper()));
                    if (user.Count > 0)
                        return Response("Usuário já cadastrado");
                    await _usuarioRepository.Adicionar(usuario);

                    return Response(usuario);
                }

                else
                    return Response("Erro ao realizar Operação", false);
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }
        [HttpPost("GerarToken")]
        [AllowAnonymous]
        public async Task<IActionResult> GerarToken(UsuarioModel usuario)
        {
            try
            {
               var token = await Task.Run(() => { return Token.GerarToken(usuario); });
                return Response(token);
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

    }
}
