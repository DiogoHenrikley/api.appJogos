﻿using appJogos.Controllers;
using appJogos.Domain.Model;
using appJogos.Repository.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace appJogos
{
    [Route("api/[Controller]")]
    [ApiController]
    [Authorize]
    public class AmigosController : BaseController
    {
        private readonly IAmigosRepository _amigosRepository;

        public AmigosController(IAmigosRepository amigosRepository)
        {
            _amigosRepository = amigosRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Adicionar([FromBody] AmigoModel entity)
        {
            try
            {
                entity.GerarId();
                return Response(await _amigosRepository.Adicionar(entity));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpPut]
        public async Task<IActionResult> Atualiza([FromBody] AmigoModel entity)
        {
            try
            {
                return Response(await _amigosRepository.Atualizar(entity));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpGet("obterPorDescricao/{Descricao}")]
        public async Task<IActionResult> ObterPorDescricao(string Descricao)
        {
            try
            {
                return ResultadoPesquisa(await _amigosRepository.Buscar(b => b.Nome.ToUpper().Contains(Descricao.ToUpper())));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> ObterPorId(int Id)
        {
            try
            {
                return Response(await _amigosRepository.ObterPorId(Id));

            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }
        [HttpGet]
        public async Task<IActionResult> ObterTodos()
        {
            try
            {
                return ResultadoPesquisa(await _amigosRepository.ObterTodos());
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }
        [HttpDelete("{Id}")]
        public async Task<IActionResult> Remover(int Id)
        {

            try
            {
                var entidade = await _amigosRepository.ObterPorId(Id);
                return Response(await _amigosRepository.Remover(entidade));
            }
            catch (Exception ex)
            {
                return ResponseErro(ex.Message.ToString());
            }
        }
    }
}
