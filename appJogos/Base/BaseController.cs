﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace appJogos.Controllers
{
    public class BaseController : Controller
    {
        protected new IActionResult Response(object Result = null, bool success = true)
        {
            return Ok(new { Success = success, Dados = Result });
        }

        protected IActionResult ResultadoPesquisa<T>(IEnumerable<T> dados)
        {
            return Ok(new { Success = true, Dados = dados, Quantidade = dados.Count() });
        }

        protected IActionResult ResponseErro(string ex)
        {
            return BadRequest(new { Success = false, Erro = ex, StatusCode = $"{StatusCodes.Status500InternalServerError} - Erro no processamento, avise a equipe técnica." });
        }
    }
}
