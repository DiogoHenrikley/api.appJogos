﻿using appJogos.Domain.Model;
using appJogos.Repository.Data;
using appJogos.Repository.Repository.Interface;
using appJogos.Repository.Repository.RepositoryBase;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Class
{
    public class AmigosRepository : RepositoryBase<AmigoModel>, IAmigosRepository
    {
        private readonly DataContext _dataContext;

        public AmigosRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public override async Task<bool> Adicionar(AmigoModel entity)
        {
            return await base.Adicionar(entity);
        }

        public override async Task<bool> Atualizar(AmigoModel entity)
        {
            return await base.Atualizar(entity);
        }

        public override async Task<List<AmigoModel>> Buscar(Expression<Func<AmigoModel, bool>> predicado)
        {
            return await base.Buscar(predicado);
        }

        public override async Task<List<AmigoModel>> ObterPorDescricao(string Descricao)
        {
            return await base.ObterPorDescricao(Descricao);
        }

        public override async Task<AmigoModel> ObterPorId(int Id)
        {
            return await base.ObterPorId(Id);
        }

        public override async Task<List<AmigoModel>> ObterTodos()
        {
            return await base.ObterTodos();
        }

        public override async Task<bool> Remover(AmigoModel entity)
        {
            return await base.Remover(entity);
        }

        public async Task<UsuarioModel> Autenticar(string Email, string Senha)
        {
            IQueryable<UsuarioModel> query = _dataContext.Usuarios.Where(b => b.Email.ToUpper().Equals(Email.ToUpper())
                && b.Senha.Equals(Senha));
            return await query.AsNoTracking().FirstOrDefaultAsync();
        }
    }
}
