﻿using appJogos.Domain.Model;
using appJogos.Repository.Data;
using appJogos.Repository.Repository.Interface;
using appJogos.Repository.Repository.RepositoryBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace appJogos.Repository.Repository.Class
{
    public class UsuarioRepository : RepositoryBase<UsuarioModel>, IUsuarioRepository
    {
        private readonly DataContext _dataContext;
        public UsuarioRepository(DataContext datacontext) : base(datacontext)
        {
            _dataContext = datacontext;
        }

        public override Task<bool> Adicionar(UsuarioModel entity)
        {
            return base.Adicionar(entity);
        }

        public async Task<UsuarioModel> Autenticar(string Email, string Senha)
        {
            IQueryable<UsuarioModel> query =  _dataContext.Usuarios.Where(b => b.Email.ToUpper().Equals(Email.ToUpper()) &&
                                                          Senha.Equals(Senha));
            return await query.AsNoTracking().FirstOrDefaultAsync();
        }

        public override Task<List<UsuarioModel>> Buscar(Expression<Func<UsuarioModel, bool>> predicado)
        {
            return base.Buscar(predicado);
        }

    }
}
