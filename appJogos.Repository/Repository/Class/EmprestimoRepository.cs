﻿using appJogos.Domain.Model;
using appJogos.Repository.Data;
using appJogos.Repository.Repository.Interface;
using appJogos.Repository.Repository.RepositoryBase;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Class
{
    public class EmprestimoRepository : RepositoryBase<EmprestimoModel>, IEmprestimoRepository
    {
        private readonly DataContext _dataContext;
        public EmprestimoRepository(DataContext datacontext) : base(datacontext)
        {
            _dataContext = datacontext;
        }

        public override async Task<bool> Adicionar(EmprestimoModel entity)
        {
            return await base.Adicionar(entity);
        }

        public override async Task<bool> Atualizar(EmprestimoModel entity)
        {
            return await base.Atualizar(entity);
        }

        public override async Task<List<EmprestimoModel>> Buscar(Expression<Func<EmprestimoModel, bool>> predicado)
        {
            return await base.Buscar(predicado);
        }

        public override async Task<List<EmprestimoModel>> ObterPorDescricao(string Descricao)
        {
            return await base.ObterPorDescricao(Descricao);
        }

        public override async Task<EmprestimoModel> ObterPorId(int Id)
        {
            return await base.ObterPorId(Id);
        }

        public override async Task<List<EmprestimoModel>> ObterTodos()
        {
            IQueryable<EmprestimoModel> query = _dataContext.Emprestimo
                                                .Include(b => b.Amigo)
                                                .Include(b => b.Jogos);

            return await query.AsNoTracking().ToListAsync();
        }

        public override async Task<bool> Remover(EmprestimoModel entity)
        {
            return await base.Remover(entity);
        }
    }
}
