﻿using appJogos.Domain.Model;
using appJogos.Repository.Data;
using appJogos.Repository.Repository.Interface;
using appJogos.Repository.Repository.RepositoryBase;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Class
{
    public class JogosRepository :  RepositoryBase<JogosModel>, IJogosRepository
    {
        private readonly DataContext _datacontext;

        public JogosRepository(DataContext dbJogosContext):base(dbJogosContext)
        {
            _datacontext = dbJogosContext;

        }
        public override async  Task<bool> Adicionar(JogosModel entity)
        {
            return await base.Adicionar(entity);
        }

        public override async Task<bool> Atualizar(JogosModel entity)
        {
            return await base.Atualizar(entity);
        }

        public override async Task<List<JogosModel>> Buscar(Expression<Func<JogosModel, bool>> predicado)
        {
            return await base.Buscar(predicado);
        }

        public override async Task<List<JogosModel>> ObterPorDescricao(string Descricao)
        {
            return await base.ObterPorDescricao(Descricao);
        }

        public override async Task<JogosModel> ObterPorId(int Id)
        {
            return await base.ObterPorId(Id);
        }

        public override async Task<List<JogosModel>> ObterTodos()
        {
            return await base.ObterTodos();
        }

        public override async Task<bool> Remover(JogosModel entity)
        {
            return await base.Remover(entity);
        }
    }
}

