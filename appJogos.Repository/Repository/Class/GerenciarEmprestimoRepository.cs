﻿using appJogos.Domain.Model;
using appJogos.Repository.Data;
using appJogos.Repository.Repository.Interface;
using appJogos.Repository.Repository.RepositoryBase;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Class
{
    public class GerenciarEmprestimoRepository : RepositoryBase<GerenciarEmprestimoModel>, IGerenciarEmprestimoRepository
    {
        private readonly DataContext _dataContext;

        public GerenciarEmprestimoRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }
        public override async Task<bool> Adicionar(GerenciarEmprestimoModel entity)
        {
            return  await base.Adicionar(entity);
        }

        public override async Task<bool> Atualizar(GerenciarEmprestimoModel entity)
        {
            return  await base.Atualizar(entity);
        }

        public override async Task<List<GerenciarEmprestimoModel>> Buscar(Expression<Func<GerenciarEmprestimoModel, bool>> predicado)
        {
            return await base.Buscar(predicado);
        }

        public async Task<GerenciarEmprestimoModel> ObterJogoPorId(int Id)
        {
            IQueryable<GerenciarEmprestimoModel> query = _dataContext.GerenciamentoEmprestimo
                                           .Include(b => b.Jogos)
                                           .Where(b => b.JogoId.Equals(Id));
            return await query.AsNoTracking().FirstOrDefaultAsync();

        }

        public override async Task<List<GerenciarEmprestimoModel>> ObterPorDescricao(string Descricao)
        {
            IQueryable<GerenciarEmprestimoModel> query = _dataContext.GerenciamentoEmprestimo
                                                         .Where(b => b.Amigo.Nome.ToUpper().Contains(Descricao.ToUpper()) ||
                                                                     b.Jogos.Nome.ToUpper().Contains(Descricao.ToUpper()))
                                                         .Include(b => b.Amigo)
                                                         .Include(b => b.Jogos);
            return await query.AsNoTracking().ToListAsync();
        }

        public override async Task<GerenciarEmprestimoModel> ObterPorId(int Id)
        {
            IQueryable<GerenciarEmprestimoModel> query = _dataContext.GerenciamentoEmprestimo
                                                         .Where(b => b.Id.Equals(Id))
                                                         .Include(b => b.Amigo)
                                                         .Include(b => b.Jogos);

            return await query.AsNoTracking().FirstOrDefaultAsync();
        }

        public override async Task<List<GerenciarEmprestimoModel>> ObterTodos()
        {
            IQueryable<GerenciarEmprestimoModel> query = _dataContext.GerenciamentoEmprestimo
                                                          .Include(b => b.Amigo)
                                                          .Include(b => b.Jogos);
            return await query.AsNoTracking().ToListAsync();
        }

        public override async Task<bool> Remover(GerenciarEmprestimoModel entity)
        {
            return await base.Remover(entity);
        }
    }
}
