﻿using appJogos.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Interface
{
    public interface IUsuarioRepository : IRepositoryBase<UsuarioModel>
    {
        Task<UsuarioModel> Autenticar(string Email, string Senha);
    }
}
