﻿using appJogos.Domain.Model;

namespace appJogos.Repository.Repository.Interface
{
    public interface IEmprestimoRepository :IRepositoryBase<EmprestimoModel>
    {
    }
}
