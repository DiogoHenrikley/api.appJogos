﻿using appJogos.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace appJogos.Repository.Repository.Interface
{
    public interface IJogosRepository : IRepositoryBase<JogosModel>
    {
    }
}
