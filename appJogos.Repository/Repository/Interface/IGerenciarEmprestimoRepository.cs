﻿using appJogos.Domain.Model;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Interface
{
    public interface IGerenciarEmprestimoRepository : IRepositoryBase<GerenciarEmprestimoModel>
    {
        Task<GerenciarEmprestimoModel> ObterJogoPorId(int Id);
    }
}
