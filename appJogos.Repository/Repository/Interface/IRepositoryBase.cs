﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.Interface
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        Task<bool> Adicionar(TEntity entity);
        Task<bool> Atualizar(TEntity entity);
        Task<bool> Remover(TEntity entity);
        Task<List<TEntity>> ObterTodos();
        Task<TEntity> ObterPorId(int Id);
        Task<List<TEntity>> ObterPorDescricao(string Descricao);
        Task<List<TEntity>> Buscar(Expression<Func<TEntity, bool>> predicado);
    }
}
