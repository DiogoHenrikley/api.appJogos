﻿using appJogos.Domain.Model;

namespace appJogos.Repository.Repository.Interface
{
    public interface IAmigosRepository : IRepositoryBase<AmigoModel>
    {
    }
}
