﻿using appJogos.Repository.Data;
using appJogos.Repository.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace appJogos.Repository.Repository.RepositoryBase
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        private readonly DataContext _datacontext;

        public RepositoryBase(DataContext datacontext)
        {
            _datacontext = datacontext;
        }

        public virtual async Task<bool> Adicionar(TEntity entity)
        {
            _datacontext.Add(entity);
            return await _datacontext.SaveChangesAsync() > 0;
        }

        public virtual  async Task<bool> Atualizar(TEntity entity)
        {
            _datacontext.Update(entity);
            return await _datacontext.SaveChangesAsync() > 0;
        }

        public virtual async Task<List<TEntity>> Buscar(Expression<Func<TEntity, bool>> predicado)
        {
            return await _datacontext.Set<TEntity>().Where(predicado).ToListAsync();
        }

        public virtual async Task<List<TEntity>> ObterPorDescricao(string Descricao)
        {
            return await Buscar(b => b.GetType().Name.Contains(Descricao));
        }

        public virtual async Task<TEntity> ObterPorId(int Id)
        {
            return await _datacontext.Set<TEntity>().FindAsync(Id);
        }

        public virtual async Task<List<TEntity>> ObterTodos()
        {
            return await _datacontext.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<bool> Remover(TEntity entity)
        {
            _datacontext.Remove(entity);
            return await _datacontext.SaveChangesAsync() > 0;
        }
    }
}
