﻿using appJogos.Domain.Model;
using appJogos.Repository.EntityConfig;
using Microsoft.EntityFrameworkCore;

namespace appJogos.Repository.Data
{
    public class DataContext : DbContext
    {
        #region "Configurações"
        public DbSet<AmigoModel> Amigo { get; set; }
        public DbSet<JogosModel> Jogos { get; set; }
        public DbSet<UsuarioModel>  Usuarios { get; set; }
        public DbSet <GerenciarEmprestimoModel> GerenciamentoEmprestimo { get; set; }
        public DbSet <EmprestimoModel> Emprestimo { get; set; }
        #endregion  
        public DataContext( DbContextOptions<DataContext> options ) :  base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AmigoConfiguration());
            modelBuilder.ApplyConfiguration(new JogosConfiguration());
            modelBuilder.ApplyConfiguration(new UsuarioConfiguration());
            modelBuilder.ApplyConfiguration(new GerenciarEmprestimoConfiguration());
            modelBuilder.ApplyConfiguration(new EmprestimoConfiguration());
        }
    }
}
