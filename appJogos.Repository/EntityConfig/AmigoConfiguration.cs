﻿using appJogos.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace appJogos.Repository.EntityConfig
{
    public class AmigoConfiguration : IEntityTypeConfiguration<AmigoModel>
    {
        public void Configure(EntityTypeBuilder<AmigoModel> builder)
        {
            builder.HasKey(b => b.Id);
        }
    }
}
