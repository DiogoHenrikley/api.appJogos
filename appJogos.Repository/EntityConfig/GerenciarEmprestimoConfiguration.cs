﻿using appJogos.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace appJogos.Repository.EntityConfig
{
    public class GerenciarEmprestimoConfiguration : IEntityTypeConfiguration<GerenciarEmprestimoModel>
    {
        public void Configure(EntityTypeBuilder<GerenciarEmprestimoModel> builder)
        {
            builder.HasKey(b => b.Id);
        }
    }
}
