﻿using System;

namespace appJogos.Shared
{
    public static class Util
    {

        public static int GerarId()
        {
            return new Random().Next(0, int.MaxValue);
        }
    }
}
