﻿namespace appJogos.Shared.Enumeradores
{
    public enum EnumEstiloJogos
    {
        Acao = 0,
        Aventura,
        Corrida,
        Futebol,
        Guerra,
        Terror,
        Outros
    }
}
