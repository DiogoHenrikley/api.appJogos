﻿using System.ComponentModel;

namespace appJogos.Shared.Enumeradores
{
    public enum EnumSexo
    {
        [Description("Masculino")]
        Masculino = 1,
        [Description("Feminino")]
        Feminino = 2
    }
}
